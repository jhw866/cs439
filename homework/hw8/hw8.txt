1. Yes the system call for open is necessary because there needs to be some kind of protection for files. A consquence of not having it would be that malicious code could be inserted with greater ease, and also puts the system at a higher risk by having certain files being opened.

2. it would make 3 instances of opening the file or directory. When the dup2 command occurs fd2 gives fd3 a pointer to fd2's instance of the open file or directory. So when c2 reads, we are reading from fd2's instance, and when c3 reads, we are reading the next item in fd2's instance.

3. Program is attached as hw8.c
