#include <stdio.h>
#include <dirent.h>

int main(int argc, char *argv[]) {
	char *c = argv[1];		// Get directory name
	DIR *openD = opendir(c);	// open directory
	struct dirent *d;
	while((d = readdir(openD)) != NULL) {
		printf("Inode = %i\n",(int) d->d_ino);
		printf("Name = %s\n", d->d_name);
	}
	closedir(openD);
	return 0;	
} 
