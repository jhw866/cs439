#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void dummyMethod(void) {
	int i = 1 + 2;
	int j = 2 * 2;
	int f = 3 / 5;
}

int main() {
	double acc;
	struct timespec start, stop;

	// Do a system call calculation
	clock_gettime(CLOCK_REALTIME, &start);
	getpid();
	clock_gettime(CLOCK_REALTIME, &stop);
	acc = stop.tv_nsec - start.tv_nsec;
	printf("Time for pid call = %fns\n", acc);

	// Do a function call
	clock_gettime(CLOCK_REALTIME, &start);
	dummyMethod();
	clock_gettime(CLOCK_REALTIME, &stop);
	acc = stop.tv_nsec - start.tv_nsec;

	printf("Time for dummy function call = %fns\n", acc);

	return 0;
}
