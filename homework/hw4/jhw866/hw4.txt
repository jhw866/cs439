Question 1:
	producer
	// down for empty (if empty is 0, it would wait for consumer to run, then it would run)
	// down for mutex
	// enqueue item
	// up for mutex
	// up for full
	
	consumer
	// down for full (if full is equal to max, it would wait for producer to run, then it would run)
	// down for mutex
	// enqueue item
	// up for mutex
	// up for empty
Question 2:

	Advantage: Fault-tolerance: If a thread dies, the entire processes can die because the data might be corrputed because of shared memory, compared to a process which wouldn't have shared memory and wouldn't cause a crash
	Disadvantage: Resource-Inefficient: Processes typically take up more memory and do not share their memory, while threads share a small amount of memory.

Question 3:

	If you called a wait() outside a synchronized block you would get an illegalmonitorexception. Why it does this is to make sure the  information and data is synched. If the runtime behaved differently, we could have a possiblity of deadlock.
